/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import model.Product;

/**
 *
 * @author PC
 */
public class TestInsertProduct {

    public static void main(String[] args) {
       Connection con = null;
         Database db = Database.getInstance();
         con = db.getConnection();
        try {
            String sql = "INSERT INTO PRODUCT (PD_NAME,PD_PRICE)VALUES (?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            Product product = new Product(-1 , "Cappuccino",40);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            int row = stmt.executeUpdate();
            System.out.println("Affect row: "+row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
               id = result.getInt(1);
            }
             System.out.println("Affect row: "+row+" id: "+id);
        } catch (SQLException ex) {
            System.out.println("ERROR : SQL");
        }
         db.close();
    }
}
