/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author PC
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection con = null;
         Database db = Database.getInstance();
         con = db.getConnection();
        try {
            String sql = "SELECT PD_ID,PD_NAME,PD_PRICE FROM PRODUCT" ;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("PD_ID");
                String name =result.getString("PD_NAME");
                double price = result.getDouble("PD_PRICE");
                Product product = new Product(id ,name ,price);
                System.out.println(product);
            }
        }catch (SQLException ex){
            System.out.println("ERROR : SQL");
        }
      db.close();
    }
}
